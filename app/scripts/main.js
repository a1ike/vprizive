/* function hideAllError(str) { return true; }
window.onerror = hideAllError; */

$(document).ready(function () {

  $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
        &&
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000, function () {
            // Callback after animation
            // Must change focus!
            var $target = $(target);
            if ($target.is(':focus')) { // Checking if the target was focused
              return false;
            } else {
              $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
            };
          });
        }
      }
    });

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });

  $('.v-header__toggle').on('click', function (e) {
    e.preventDefault();

    $('.v-header__menu').slideToggle('fast');
  });

  $('.v-header__close').on('click', function (e) {
    e.preventDefault();

    $('.v-header__menu').slideToggle('fast');
  });

  new Swiper('.v-how__cards', {
    spaceBetween: 30,
    navigation: {
      nextEl: '.v-how .swiper-button-next',
      prevEl: '.v-how .swiper-button-prev',
    },
    slidesPerView: 1,
    spaceBetween: 30,
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
    }
  });

  var swiper = new Swiper('.v-year__cards', {
    spaceBetween: 100,
    loop: true,
    navigation: {
      nextEl: '.v-year .swiper-button-next',
      prevEl: '.v-year .swiper-button-prev',
    },
  });

  new Swiper('.v-about__cards', {
    spaceBetween: 120,
    navigation: {
      nextEl: '.v-about .swiper-button-next',
      prevEl: '.v-about .swiper-button-prev',
    },
  });

  swiper.on('slideChange', function () {

    if (this.realIndex === 0) {
      $('.v-year__image').removeClass('v-year__image_2');
      $('.v-year__image').removeClass('v-year__image_3');
      $('.v-year__image').addClass('v-year__image_1');

    } else if (this.realIndex === 1) {
      $('.v-year__image').removeClass('v-year__image_1');
      $('.v-year__image').removeClass('v-year__image_3');
      $('.v-year__image').addClass('v-year__image_2');

    } else if (this.realIndex === 2) {
      $('.v-year__image').removeClass('v-year__image_1');
      $('.v-year__image').removeClass('v-year__image_2');
      $('.v-year__image').addClass('v-year__image_3');

    } else {
      $('.v-year__image').removeClass('v-year__image_1');
      $('.v-year__image').removeClass('v-year__image_3');
      $('.v-year__image').addClass('v-year__image_2');
    }
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.v-modal__content').hide();
    $('.v-modal__title').html($(this).data('name'));
    $('.v-modal__phone').html($(this).data('phone'));
    $('.v-modal__address').html($(this).data('address'));

    var content = $('.v-modal__content').detach();
    $(this).parent().append(content);

    $('.v-modal__content').show();
  });

  $('.v-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.v-modal__content').toggle();
  });

  /* $('.v-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'v-modal__centered') {
      $('.v-modal').toggle();
    }
  }); */

  $('.open-popup').on('click', function (e) {
    e.preventDefault();
    $('.v-popup').toggle();
  });

  $('.v-popup__close').on('click', function (e) {
    e.preventDefault();
    $('.v-popup').toggle();
  });

  $('.v-popup__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'v-popup__centered') {
      $('.v-popup').toggle();
    }
  });

  $('.open-place').on('click', function (e) {
    e.preventDefault();
    $('.v-place').toggle();
  });

  $('.v-place__close').on('click', function (e) {
    e.preventDefault();
    $('.v-place').toggle();
  });

  $('.v-place__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'v-place__centered') {
      $('.v-place').toggle();
    }
  });

  $('.v-place-card__names a').on('click', function (e) {
    e.preventDefault();
    $('.v-header__city-link').html($(this).data('name'));
    $('.v-place').toggle();
  });

  $(window).on('load resize scroll', function (e) {

    var viewportWidth = $(window).width();

    if (viewportWidth < 1200) {

      var contentCards = $('.v-plan-card').detach();
      $('.v-plan_help').append(contentCards);
      var contentAny = $('.v-plan_right').detach();
      $('.v-plan_help').append(contentAny);
    }
    if (viewportWidth < 769) {

      new Swiper('.v-help__swipes', {
        autoplay: {
          delay: 2000,
        },
        loop: true,
        pagination: {
          el: '.v-help__swipes .swiper-pagination',
        },
      });

      var contentImage = $('.v-win__image').detach();
      $('.v-win__title').append(contentImage);

      var content1 = $('.v-footer__what').detach();
      $('.v-footer__social').after(content1);

      var content2 = $('.v-footer__policy').detach();
      $('.v-footer__what').after(content2);

      var monCity = $('.v-header__city').detach();
      $('.v-header__menu').append(monCity);
    }
  });

  window.onscroll = function () { scrollFunction() };

  function scrollFunction() {
    if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
      $('.v-header').addClass('v-header_fixed');
    } else {
      $('.v-header').removeClass('v-header_fixed');
    }
  }

  new Typed('#typed', {
    strings: ['Срочная юридическая помощь в получении военного билета', 'Скажи армии НЕТ'],
    typeSpeed: 50,
    backDelay: 1000,
    startDelay: 1000,
    loop: true
  });
});
